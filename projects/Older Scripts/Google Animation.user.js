// ==UserScript==
// @name         Google Animation
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Creates "funny" Animation when you visit Google.com
// @author       You
// @match        https://www.google.com/
// @grant        GM_addStyle
// ==/UserScript==

(function(){
    //Animation CSS with GM_addStyle
    GM_addStyle("@keyframes slideinL { from { margin-left: 100%; width: 300%; } to {margin-left: absolute;  width: absolute; }}");
    GM_addStyle("@keyframes slideinR { from { margin-right: 100%; width: 300%; } to {margin-right: absolute; width: absolute; }}");
    GM_addStyle("@keyframes slideinT { from { margin-top: 100% } to { margin-top: absolute }}");
    GM_addStyle("@keyframes slideinRO { from { margin-top: 0%; margin-right: 0%;   } to { margin-top: 10%;margin-right: 10%;   }}");
    GM_addStyle("  transform:rotate(180deg); -ms-transform:rotate(180deg); -webkit-transform:rotate(180deg);");
    GM_addStyle("@keyframes spin { 100% {   transform: rotate(360deg);} }");
    GM_addStyle(" #fbar {animation: slideinL 7s 1;");
    GM_addStyle(" .gb_T {animation: slideinR 7s 1;");
    GM_addStyle(" #hplogo {animation: slideinT 7s 1;");
    //Getting Elements
    var count = 0;
    var googleSearchButton = document.getElementsByName("btnK");
    var luckyButtonElm = document.getElementById("gbqfbb");
    var luckyButtonElm2 = document.getElementById("tsf");
    var googleLogo = document.getElementById("hplogo");
    function stopAllAnimations() {
        GM_addStyle(" * {/*CSS transitions*/ -o-transition-property: none !important; -moz-transition-property: none !important; -ms-transition-property: none !important; -webkit-transition-property: none !important; transition-property: none !important; /*CSS transforms*/ -o-transform: none !important; -moz-transform: none !important; -ms-transform: none !important; -webkit-transform: none !important; transform: none !important; /*CSS animations*/ -webkit-animation: none !important; -moz-animation: none !important; -o-animation: none !important; -ms-animation: none !important; animation: none !important; }");
    }
    googleLogo.onmouseover = function bigImg(x) {
        setTimeout(function() {  GM_addStyle(" #hplogo  {animation:spin 1s   infinite;");}, 150);
    };
    googleLogo.onmouseout=function normalImg(x) {
        setTimeout(function() {    GM_addStyle(" #hplogo  {animation:spin 0s  2;");}, 150);
    };
    //  luckyButtonElm.value = "I changed the button names";
    //    luckyButtonElm2.removeChild();
    luckyButtonElm.parentNode.removeChild(luckyButtonElm);
    googleSearchButton[0].style.color = "#ff0000";
    googleSearchButton[0].style.border = "1px solid black";
    googleSearchButton[0].value = "Do not click me!";
    googleSearchButton[0].style.fontFamily = "Roboto";
    googleSearchButton[0].style.fontSize = "x-large";
    googleSearchButton[0].onmouseover = function () {
        googleSearchButton[0].style.color = "black";
    };
    googleSearchButton[0].onmouseout = function () {
        googleSearchButton[0].style.color = "red";
    };
    googleSearchButton[0].onclick = function(){
        switch(count){
            case 0:
                googleSearchButton[0].value = "Ouch!";
                count++;
                break;
            case 1:  googleSearchButton[0].value = "Stop!!";
                count++;
                break;
            case 2: googleSearchButton[0].value = "What are you doing!?!";
                count++;
                break;
            case 3:  googleSearchButton[0].value = "I'm warning you...";
                count++;
                break;
            case 4: googleSearchButton[0].value = "Fine, You asked for it! ";
                count++;
                break;
            case 5: alert("Take this!");
                googleSearchButton[0].value = " ʇɐɥʇ ǝʞᴉl noʎ op ʍoH";
                GM_addStyle(" body {transform:rotate(180deg);}");
                count++;
                break;
            case 6: alert("Back for more?");
                googleSearchButton[0].value = " Try and click me now";
                GM_addStyle(" body {transform:rotate(0deg);}");
                GM_addStyle("  .jsb {width: 20px;height: 20px;transform: scale(0.35);}");
                count++;
                break;
            case 7:
                alert("Fine I'm out of here, *Pushes Break Page Button*");
                GM_addStyle(" body {animation:spin .1s linear infinite;");
                setTimeout(beginSpinning, 4000);
                count++;
                break;
        }
    };
    function beginSpinning() {
        alert("Spin System Failing");
        alert("OH SHOOT!");
        GM_addStyle(" body {animation:spin .01s linear infinite;");
        setTimeout(beginSpinningFaster, 3000);
    }
    function beginSpinningFaster() {
        alert("Spin Recovering Failed, Over Heating");
        alert("NO NO NO!");
        GM_addStyle(" body {animation:spin .001s linear infinite;");
        setTimeout(beginSpinningMoreFaster, 5000);
    }
    function beginSpinningMoreFaster() {
        alert("IT'S GONNA BLOW!");
        GM_addStyle(" * {animation:spin .0001s linear infinite;");
        setTimeout(breakScreen, 5000);
    }
    function breakScreen() {
        googleSearchButton[0].value = "Button disabled";
        alert("BOOM!");
        stopAllAnimations();
        setTimeout(function writeBrokenGoogleImage() {document.write('<img   src="https://i.imgur.com/aCgV3wJ.jpg"  >');}, 1);
    }
})();
