// ==UserScript==
// @name         Tenrox Bucket Lister
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Creates a formatted table that outlines your Bucket information in a more readable form
// @author       Timothy Field
// @match        https://lyonscg.tenrox.net/*
// @grant        GM_addStyle
// ==/UserScript==

(function() {
    $( document ).ajaxComplete(function( event, xhr, settings ) { //<--IMPORTANT: elements will return null without this (Waits for ajax to finish)-->

        console.log("Ajax is Finished and Iframe elements should now be loaded");
        var totalTimeSheetHours = document.getElementById("TaskTimeEntry_Body_2_1");
        var totalTimes =  totalTimeSheetHours.innerHTML;
        totalTimes = totalTimes.split(" ");
        var totalHourArray = [];
        var tempString = "";
        for (let i =0; i < totalTimes.length-2;i++){
            if(totalTimes[i].includes(':')){
                tempString = totalTimes[i].substring(totalTimes[i].indexOf(">")+1,totalTimes[i].indexOf("<"));
                if (tempString.length > 0){
                    totalHourArray.push(tempString);
                }
            }
        }
       //Debug: console.log(totalHourArray);
        var timesheetControls = document.getElementById('TSControl1Data');
        var timeSheetBucketsArray = [];
        var timeSheetString = timesheetControls.innerHTML;
        var assignmentBuckets =[];
        timeSheetBucketsArray  = timeSheetString.split('" ');
        for (let i =0; i < timeSheetBucketsArray.length;i++){

            if   (timeSheetBucketsArray[i].includes("PROJECT_NAME") || timeSheetBucketsArray[i].includes("ASS_NAME") ||timeSheetBucketsArray[i].includes("CLIENT_NAME") || timeSheetBucketsArray[i].includes("PMGRFN") || timeSheetBucketsArray[i].includes("PMGRLN")  || timeSheetBucketsArray[i].includes("PMGRE")){
                assignmentBuckets.push(timeSheetBucketsArray[i] + '"');
            }
        }

        var allBucketsHeaderElement = document.createElement("h2");
        allBucketsHeaderElement.innerHTML = " ALL BUCKETS IN ORDER <hr>";
        allBucketsHeaderElement.style.textAlign = "center";
        var allBucketsTableElement = document.createElement("table");
        allBucketsTableElement.className = "customTable";
        allBucketsTableElement.style.fontSize = "xx-large";
        allBucketsTableElement.style.backgroundColor = "#EEEEEE";
        var allBucketStr = "";
        var columnCount = 0;
        var spaceStr = "&nbsp; &nbsp; &nbsp;";
        var tableHeaders = "Task,Project Name,Manager Email,Manager First Name,Manager Last Name,Client Name,Total Time";
        var tableHeadersArr = tableHeaders.split(",");
        var columnText = "";
        var tr = document.createElement("tr");

        //Generate Table Headers
        for (let k =0; k < tableHeadersArr.length; k++){
            var th = document.createElement("th");
            th.className = "customTable";
            columnText =document.createTextNode(tableHeadersArr[k]);
            th.appendChild(columnText);
            tr.appendChild(th);
        }
        allBucketsTableElement.appendChild(tr);
        var bucketNumber=0;
        for (let j=0; j < assignmentBuckets.length; j++){

            switch(columnCount){ //Goes column by column creating table data and append them to the row, then resets at the end after adding the row to the table
                case 0: //Task
                    tr = document.createElement("tr");
                    tr.className = "customTable";
                    var td = document.createElement("td");
                    td.className = "customTable";
                    allBucketStr = allBucketStr  +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"')) ;
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    td.appendChild(columnText);
                    tr.appendChild(td);
                    allBucketStr = "";
                    columnCount++;
                    break;
                case 1: //Project Name
                    td = document.createElement("td");
                    td.className = "customTable";
                    allBucketStr = allBucketStr  +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"'));
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    td.appendChild(columnText);
                    tr.appendChild(td);
                    allBucketStr = "";
                    columnCount++;
                    break;
                case 2: // Manager Email
                    td = document.createElement("td");
                    td.className = "customTable";
                    allBucketStr = allBucketStr +"" +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"'));
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    td.appendChild(columnText);
                    tr.appendChild(td);
                    allBucketStr = "";
                    columnCount++;
                    break;
                case 3: //Manager First Name
                    td = document.createElement("td");
                    td.className = "customTable";
                    allBucketStr = allBucketStr + "" +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"')) ;
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    td.appendChild(columnText);
                    tr.appendChild(td);
                    allBucketStr = "";
                    columnCount++;
                    break;
                case 4://Manager Last Name
                    td = document.createElement("td");
                    td.className = "customTable";
                    allBucketStr = allBucketStr + " " +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"'));
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    td.appendChild(columnText);
                    tr.appendChild(td);
                    allBucketStr = "";
                    columnCount++;
                    break;
                case 5: //Client and Total Time
                    td = document.createElement("td");
                    var tdTime = document.createElement("td");
                    var txtTimeString ="" + totalHourArray[bucketNumber++];
                    td.className = "customTable";
                    tdTime.className = "customTable";
                    allBucketStr = allBucketStr + "   " +assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"')) ;
                    allBucketStr = allBucketStr.replaceAll('"','');
                    columnText = document.createTextNode(allBucketStr);
                    var txtTime = document.createTextNode(txtTimeString);
                    td.appendChild(columnText);
                    tdTime.appendChild(txtTime);
                    tr.appendChild(td);
                    tr.appendChild(tdTime);
                    allBucketsTableElement.appendChild(tr);
                    allBucketStr = "";
                    columnCount=0;
                    break;
                default:
                    allBucketStr = allBucketStr + assignmentBuckets[j].substring(assignmentBuckets[j].indexOf('"')) + "  ";
                    columnCount++;
            }
        }

        //Adds Nice style/format to the table with class "customTable"
        GM_addStyle('table.customTable { font-family: arial, sans-serif; border-collapse: collapse; width: 98%; padding: 15px; text-align: justify;} td.customTable, th.customTable { border: 2px solid darkgray; text-align: center; padding: 8px;} tr.customTable:nth-child(even) { background-color: #dddddd;}');
        var daTaskElm = document.getElementById("daTaskTimeEntry");
        var taskTimeEntryHeader = document.getElementById("Left_0");
        var horizontalR = document.createElement("h2");
        horizontalR.innerHTML = "<hr>";
        daTaskElm.appendChild(allBucketsHeaderElement);
        daTaskElm.appendChild(allBucketsTableElement );
        daTaskElm.appendChild(horizontalR);

    });

})();