// ==UserScript==
// @name         Lynda.com Course Time Grabber + Theater mode
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Creates Header on Lynda Tutorial that shows Total Time of the course and creates a full screen like theater mode at the bottom of the course
// @author       You
// @match        https://www.lynda.com/*/*/*
// @grant        GM_addStyle
// ==/UserScript==

(function() {

    var elements = document.getElementsByClassName("video-duration");
    var transcripts = document.getElementsByClassName("transcript ga");
    var transcriptsStr= "";
    for (let i=0; i < transcripts.length;i++){
        transcriptsStr+= transcripts[i].innerHTML;
    }
    var min = 0.0;
    var seconds = 0.0;
    var totalMins = 0.0;
    var totalSecs =0;
    for (let i=0; i < elements.length; i++){

        if (elements[i].innerHTML.indexOf("m") == 1){
            min = Number(elements[i].innerHTML.charAt(0));
            totalMins += min;
        }
        if (elements[i].innerHTML.indexOf("m") == 2){
            min = elements[i].innerHTML.charAt(0) + elements[i].innerHTML.charAt(1);
            totalMins += Number(min);
        }
    }
    for (let i=0; i < elements.length; i++){
        var str = elements[i].innerHTML;
        seconds = elements[i].innerHTML.substring(str.lastIndexOf("m")+1,str.lastIndexOf("s"));
        if (seconds.includes("m")) {continue;}
        totalSecs += Number(seconds);
    }
    var totalTime = (totalMins *60) + totalSecs;
    totalTime = (totalTime/60)/60;
    var cleanTime = totalTime.toFixed(2).toString();
    cleanTime = "0." + cleanTime.substring(2);
    cleanTime = totalTime.toFixed(0) +" Hours " +  (Number(cleanTime)*60).toFixed(0) + " Minute(s)!";
    var fastTotalTime = totalTime * 0.75;
    var fastCleanTime = fastTotalTime.toFixed(2).toString();
    fastCleanTime = "0." + fastCleanTime.substring(2);
    fastCleanTime =  (Number(fastCleanTime)*60).toFixed(0) + " Minute(s)!";
    fastCleanTime = fastTotalTime.toString().charAt(0) + " Hours " + fastCleanTime;
    var allTimeInfoString = "<hr>This course is " + totalMins +" Minutes and " + totalSecs + " Seconds Long! \n" + "Which is about " + totalTime.toFixed(2) + " hours!\n" + cleanTime +"<br>\nAnd at 1.25x can be watched in " + fastTotalTime.toFixed(2) + " hours!\n" + fastCleanTime + "<hr>";
    GM_addStyle(" #courseplayer { transform: scale(.99,.79);} ");
    var p = document.getElementById('responsive-wrapper-main');
    var f = document.getElementById('footer');
    var d = document.getElementById('courseplayer');
    var s = document.getElementById('video-container');
    var allTimeElm = document.createElement("h1");
    f.innerHTML = '';
    allTimeElm.style.color = "white";
    allTimeElm.style.textAlign = "center";
    allTimeElm.innerHTML = allTimeInfoString;
    p.prepend(allTimeElm);
    p.appendChild(d);
    p.appendChild(s);
    window.scrollBy(0,document.height);
    s.onclick = function()
    { d.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    };
})();