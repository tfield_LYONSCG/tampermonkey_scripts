# Tampermonkey_Scripts
# Project Title 
A Collection of Tampermonkey Scripts including the Tenrox Mega Pack and other not so useful scripts

## Getting Started
You'll need to download the tampermonkey Plugin for chrome to run these scripts on your browser 
http://bit.ly/2F4NqbD

# Tenrox Mega Pack
## Quick Install
Import "Tenrox Mega Pack.zip" into Tampermonkey in the Utilities Tab under the Zip Section

## Tenrox Bucket Hider 
NOTE: This must be ran with the *Tenrox Bucket Styler* or you will not see the buttons properly
Adds functionality to hide Tenrox Buckets in order to isolate Buckets for easier viewing / comparison
## Tenrox Bucket Lister 
Creates a formatted table that outlines your Bucket information in a more readable form at the bottom of your timesheet
## Tenrox Bucket Styler
Injects CSS into your timesheet to make it more readable, optimized for 1920x1080, will be scaled at low resolutions

# Other Scripts


## Google Animation.user.js
Creates "funny" Animation when you visit Google.com

## Lynda.com Course Time Grabber + Theater mode.user.js
Creates Header on Lynda Tutorial that shows Total Time of the course and creates a full screen like theater mode at the bottom of the course



## Running the tests
Make sure you are on the correct webpage for the script (can be seen in the match tag at the top of the script).
Make sure TamperMonkey is enabled.
Make sure Proper TamperMonkey script is enabled.

## Authors
Timothy Field @ LYONSCG



