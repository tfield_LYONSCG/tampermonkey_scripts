// ==UserScript==
// @name         Tenrox Bucket Lister Version 1.1
// @namespace    http://tampermonkey.net/
// @version      1.1
// @description  Creates a formatted table that outlines your Bucket information in a more readable form at the bottom of your timesheet
// @author       Timothy Field
// @match        https://lyonscg.tenrox.net/*
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @grant        GM_addStyle
// ==/UserScript==

if (document.getElementById('frmTimeTamplate') != null) {
    var btn = document.createElement("BUTTON");
    btn.onclick = init;
    btn.innerHTML = '<h2> Tenrox Bucket Lister (BETA)</h2> ';
    btn.classList.add('bucketLister');
    var copyBtn = document.createElement("BUTTON");
    copyBtn.onclick = copy;
    copyBtn.setAttribute("id", "copyBtn");

    GM_addStyle(`
    .bucketLister {
        text-align: center;
        padding: 8px;
        background: #0090CA;
        color: white;
        box-shadow: 3px 3px 6px 0px #ccc;
        margin: 10px 15%;
        width: 50%;
        cursor:pointer;
        transition:500ms ease all;
        outline:none;
    }

    .bucketLister:hover{
        background:#fff;
        color:#0090CA;
        width: 51%;
    }

    }
    `);
    var warning = document.createElement("h3");
    warning.innerHTML = "Note: Does NOT update in real time";


    document.body.appendChild(btn);
    document.body.appendChild(warning);
}

function init() {
    btn.innerHTML = '<h2> Refresh Table With Changes </h2> ';
    var existingButton = document.getElementById('copyBtn') || "empty";
    if (existingButton != "empty") {
        $('#copyBtn').removeClass('change');
        document.body.removeChild(existingButton);
    }
    var existingTable = document.getElementById('bucketListerTable') || "empty";
    if (existingTable != "empty") {
        document.body.removeChild(existingTable);
    }
    var bucketLoader = document.createElement("div");
    var bucketListerTable = document.createElement("table");
    bucketListerTable.classList.add('customTable');
    bucketLoader.setAttribute("id", "bucketLoader");
    bucketListerTable.setAttribute("id", "bucketListerTable");
    var bucketListerTableBody = document.createElement("tbody");
    bucketListerTable.innerHTML = "";
    document.body.appendChild(bucketLoader);
    document.body.appendChild(bucketListerTable);
    bucketListerTable.appendChild(bucketListerTableBody);
    warning.style.fontWeight = "bold";
    warning.style.fontSize = "14px";
    warning.innerHTML = "Note: Does NOT update in real time (Please use Button after making changes)";

    let TaskTimeEntryBodyElementTest = document.getElementById('TaskTimeEntry_Body_0_1');
    let TaskTimeEntryBodyElementTotalsTest = document.getElementById('TaskTimeEntry_Body_2_1');

    let TSControl1 = document.getElementById('TSControl1');
    let TSControl1TDs = TSControl1.getElementsByTagName('td');

    let NonWorkingTimeEntryNames = document.getElementById('NonWorkingTimeEntry_Body_0_3') || document.createElement("table");

    let NonWorkingTimeEntry = document.getElementById('NonWorkingTimeEntry_Body_2_3') || document.createElement("table");
    let TimeTotalFooter = document.getElementById('TimeFooter_2_4') || document.getElementById('TimeFooter_2_2') || document.createElement("table");

    let TaskTimeEntryBodyElementTestTDs = TaskTimeEntryBodyElementTest.getElementsByTagName('a') || [];
    let NonWorkingTimeEntryNamesTDs = NonWorkingTimeEntryNames.getElementsByTagName('a') || [];
    let TaskTimeEntryBodyElementTotalsTestTDs = TaskTimeEntryBodyElementTotalsTest.getElementsByTagName('td') || [];
    let NonWorkingTimeEntryTDs = NonWorkingTimeEntry.getElementsByTagName('td') || [];
    let TimeTotalFooterTDs = TimeTotalFooter.getElementsByTagName('td') || [];

    let timeTotalsCount = 1;
    let tableHeaders = ["My Clients", "My Projects", "My Tasks", "Project Manager", "PM Email", "Total Time"];


    let timesheetControls = document.getElementById('TSControl1Data');
    let timeSheetBucketsArray = [];
    let timeSheetString = timesheetControls.innerHTML;
    let PMGRFN = [];
    let PMGRLN = [];
    let PMGRE = [];
    timeSheetBucketsArray = timeSheetString.split('" ');
    //document.write(timeSheetBucketsArray);
    for (let i = 0; i < timeSheetBucketsArray.length; i++) {
        //Project Manager First Name
        if (timeSheetBucketsArray[i].includes("PMGRFN")) {

            if (timeSheetBucketsArray[i].length > 8) {
                PMGRFN.push(timeSheetBucketsArray[i] + '"');
            }
        }
        //Project Manager Last Name
        if (timeSheetBucketsArray[i].includes("PMGRLN")) {
            if (timeSheetBucketsArray[i].length > 8) {
                PMGRLN.push(timeSheetBucketsArray[i] + '"');
            }
        }
        //Project Manager Email
        if (timeSheetBucketsArray[i].includes("PMGRE")) {
            if (timeSheetBucketsArray[i].length > 8) {
                PMGRE.push(timeSheetBucketsArray[i] + '"');
            }
        }
    }

    PMGRFN.push('-');
    PMGRLN.push('-');
    PMGRE.push('-');

    for (let x = 0; x < tableHeaders.length; x++) {
        var tableHeader = document.createElement("th");
        tableHeader.innerHTML = tableHeaders[x];
        bucketListerTableBody.appendChild(tableHeader);
    }
    for (let x = 0; x < TaskTimeEntryBodyElementTestTDs.length; x += 3) {
        var bucketListerTableProjectRow = document.createElement("tr");
        bucketListerTableProjectRow.innerHTML += "<td>" + (TaskTimeEntryBodyElementTestTDs[x].innerHTML) + "</td> ";
        bucketListerTableProjectRow.innerHTML += "<td>" + (TaskTimeEntryBodyElementTestTDs[x + 1].innerHTML) + "</td> ";
        bucketListerTableProjectRow.innerHTML += "<td>" + (TaskTimeEntryBodyElementTestTDs[x + 2].innerHTML) + "</td> ";
        bucketListerTableProjectRow.innerHTML += "<td>" + PMGRFN[timeTotalsCount - 1].substring(PMGRFN[timeTotalsCount - 1].indexOf('"')).replace(/['"]+/g, '') + ", " + PMGRLN[timeTotalsCount - 1].substring(PMGRLN[timeTotalsCount - 1].indexOf('"')).replace(/['"]+/g, '') + "</td> ";
        bucketListerTableProjectRow.innerHTML += "<td>" + PMGRE[timeTotalsCount - 1].substring(PMGRE[timeTotalsCount - 1].indexOf('"')).replace(/['"]+/g, '') + "</td> ";
        bucketListerTableProjectRow.innerHTML += "<td>" + (TaskTimeEntryBodyElementTotalsTestTDs[timeTotalsCount].innerHTML) + "</td> ";
        bucketListerTableBody.appendChild(bucketListerTableProjectRow);
        timeTotalsCount++;
    }
    for (let x = 0; x < NonWorkingTimeEntryNamesTDs.length; x++) {
        var bucketListerTableNonWorkingTRow = document.createElement("tr");
        bucketListerTableNonWorkingTRow.innerHTML += "<td>" + (NonWorkingTimeEntryNamesTDs[x].innerHTML) + "</td> ";
        bucketListerTableNonWorkingTRow.innerHTML += "<td> </td> ";
        bucketListerTableNonWorkingTRow.innerHTML += "<td> </td> ";
        bucketListerTableNonWorkingTRow.innerHTML += "<td> </td> ";
        bucketListerTableNonWorkingTRow.innerHTML += "<td> </td> ";
        bucketListerTableNonWorkingTRow.innerHTML += "<td>" + (NonWorkingTimeEntryTDs[x + 1].innerHTML) + "</td> ";
        bucketListerTableBody.appendChild(bucketListerTableNonWorkingTRow);
    }
    var bucketListerTableTotalRow = document.createElement("tr");
    bucketListerTableTotalRow.innerHTML += "<td> <strong> Total </strong> </td> ";
    bucketListerTableTotalRow.innerHTML += "<td> </td> ";
    bucketListerTableTotalRow.innerHTML += "<td> </td> ";
    bucketListerTableTotalRow.innerHTML += "<td> </td> ";
    bucketListerTableTotalRow.innerHTML += "<td> </td> ";
    bucketListerTableTotalRow.innerHTML += "<td> <strong> " + (TimeTotalFooterTDs[1].innerHTML) + "</strong> </td> ";
    bucketListerTableBody.appendChild(bucketListerTableTotalRow);

    GM_addStyle(`
    #bucketLoader {
        display: block;
        height: 100px;
        background-image: url(https://upload.wikimedia.org/wikipedia/commons/4/4c/Android_style_loader.gif);
        background-position: 50%;
        background-repeat: no-repeat;
        background-size: 100px 100px;
    }

    #bucketListerTable {
         margin: 0 1% 30px;
         display: none;
         width: 98%;
     }

    .customTable td,th {
         border: 1px solid darkgray;
         text-align: center;
         padding: 8px;
         font-size: 13.5px;
}

    .customTable tr:nth-child(even) {
         background-color: #d8ecf3;
}

     .customTable tr:hover {
          font-weight: bold;
          transition: all 200ms;
     }
    #copyBtn {
        position: relative;
        padding: 8px;
        background: #0090CA;
        color: white;
        box-shadow: 3px 3px 6px 0px #ccc;
        margin: 10px 15%;
        width: 50%;
        cursor:pointer;
        transition:500ms ease all;
        outline:none;
    }

    #copyBtn.change:after {
        content: attr(data-content);
        position: absolute;
        top: -30px;
        left: 0;
        width: 100%;
        font-size: 21px;
        color: black;
    }
    @media screen and (max-width: 780px) {
        .customTable td,th {
             border: 1px solid darkgray;
             text-align: center;
             padding: 4px;
             font-size: 9px;
        }
   }
`);
    // Smooth Transition and  Fade in/out with Loading gif
    $('#bucketLoader').fadeOut(200, function () {
        document.body.removeChild(bucketLoader);
    });
    $('#bucketListerTable').fadeIn(500, function () {
        bucketListerTableBody.scrollIntoView();

        copyBtn.innerHTML = '<h2> Copy Table Text </h2> ';
        document.body.appendChild(copyBtn);;

    });
}

function copy() {
    var copyTable = document.getElementById("bucketListerTable");
    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(copyTable);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(copyTable);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.warn("Could not select text in node: Unsupported browser.");
    }

    document.execCommand("copy");
    $('#copyBtn').addClass('change').attr('data-content', 'Text was copied to clipboard');
}

