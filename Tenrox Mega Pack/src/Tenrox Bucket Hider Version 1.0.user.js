// ==UserScript==
// @name         Tenrox Bucket Hider Version 1.0
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Adds functionality to hide Tenrox Buckets in order to isolate Buckets for easier viewing / comparison
// @author       Timothy Field
// @match        https://lyonscg.tenrox.net/*
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @grant        GM_addStyle
// ==/UserScript==

if (document.getElementById('frmTimeTamplate') != null) {
    var btn = document.createElement("BUTTON");
    btn.onclick = init;
    btn.innerHTML = 'Tenrox Bucket Hider (BETA) ';
    btn.classList.add('bucketLister');

    GM_addStyle(`
    .bucketLister {
        text-align: center;
        padding: 8px;
        background: #0090CA;
        color: white;
        box-shadow: 3px 3px 6px 0px #ccc;
        margin: 10px 15%;
        width: 50%;
        cursor:pointer;
        transition:500ms ease all;
        outline:none;
    }
    #NavigationLinks__Submit20612_5418346Btn {
        display:none;
    }
    .bucketLister:hover{
        background:#fff;
        color:#0090CA;
        width: 51%;

    }`);

    document.body.prepend(btn);
}
var rows;
function init() {

    rows = document.querySelectorAll('.TRColorOdd,.TRColorEven');
    var displayAtt;
    var daTaskTimeEntry = document.getElementById('daTaskTimeEntry');
    var restoreBtn = document.createElement("BUTTON");
    restoreBtn.classList.add('bucketLister');

    for (let i = 0; i < (rows.length-18) / 3; i++) {
        var hideBtn = document.createElement("BUTTON");
        displayAtt = rows[i].style.display;
        hideBtn.onclick = function () {
            rows[i].style.display = 'none';
            rows[i + rows.length / 3].style.display = 'none';
            rows[i + (2 * (rows.length / 3))].style.display = 'none';
            return false;
        }
        hideBtn.innerHTML = 'x';
        rows[i].append(hideBtn);
    }
    restoreBtn.onclick = function () {
        for (let j = 1; j < (rows.length-18)/3; j++) {
            rows[j].style.display = 'inherit';
            rows[j + rows.length / 3].style.display = 'inherit';
            rows[j + (2 * (rows.length / 3))].style.display = 'inherit';
        }
        return false;
    }
    restoreBtn.innerHTML = 'Restore all buckets';
    daTaskTimeEntry.prepend(restoreBtn);
    btn.style.display = 'none';

    var warning = document.createElement("h3");
    warning.innerHTML = "Currently running with the Tenrox Bucket Hider BETA <br> NOTE: It is also suggested to disable this script before submitting your Sheet as this has NOT been tested for that case";
    warning.style.color = 'red';
    document.body.prepend(warning);
}

