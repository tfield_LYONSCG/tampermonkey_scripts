// ==UserScript==
// @name         Tenrox Bucket Styler
// @namespace    http://tampermonkey.net/
// @version      1.0
// @description  Injects CSS into your timesheet to make it more readable, optimized for 1920x1080, will be scaled at low resolutions
// @author       Timothy Field
// @match        https://lyonscg.tenrox.net/*
// @require      http://code.jquery.com/jquery-3.3.1.min.js
// @grant        GM_addStyle
// ==/UserScript==

if (document.getElementById('frmTimeTamplate') != null) {
    GM_addStyle(`
    .TRColorEven td {
         font-weight:bold;
    }
    td {
        height: 30px !important;
        overflow: visible;
    }
    #TaskTimeEntry_Body_0_1 div {
        position: relative;
        z-index: 100;
        OVERFLOW: visible !important;
        width: 95% !important;
    }
    #TaskTimeEntry_Body_0_1 {
        width: 100% !important;
    }
    #TaskTimeEntry_Body_0_1 td {
        border: 1px solid #bbb;
        padding: 0 11px;
    }
    #TaskTimeEntry_Body_0_1 td:last-of-type {
        border-right: none;
        padding-right: 6px;
    }
    #TaskTimeEntry_Body_1_1 td {
        border: none;
    }

    #Left_0 {
        width: unset !important;
        white-space: nowrap;
        max-width: 100%;
    }
    #TSControl1 td {
        white-space: normal !important;
        text-overflow: ellipsis !important;
        max-width: 100%;
    }
    #Right_2 {
        width: 65px;
    }
    @media only screen and (max-width: 1400px) {
       #TSControl1 {
            transform: scale(0.825) translate(-10vw,-5.5vw);
        }
    }
    #NavigationLinks__Submit20612_5418346Btn {
        display:none;
    }
`);
var warning = document.createElement("h3");
warning.innerHTML = "Currently running with the Tenrox Bucket Styler BETA <br> Disable Tampermonkey Script if you have any problems viewing your timesheet <br> NOTE: It is also suggested to disable this script before submitting your Sheet as this has NOT been tested for that case";
warning.style.color = 'red';
document.body.prepend(warning);
}

